FROM centos/systemd

MAINTAINER DucCNV chauduc.1211@gmail.com

#Install repository
RUN yum -y update
RUN yum -y install epel-release
RUN yum -y localinstall http://rpms.famillecollet.com/enterprise/remi-release-7.rpm

#Add user: interdev
RUN useradd -m interdev && usermod -aG wheel interdev

#Add initscripts
RUN yum -y install initscripts && yum clean all

#Add C/C++ install
RUN yum install -y gcc

#Add wget command
RUN yum install -y wget

#Add Java
RUN su - interdev -c "wget --no-cookies --no-check-certificate --header \"Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie\" \"http://download.oracle.com/otn-pub/java/jdk/7u79-b15/jdk-7u79-linux-x64.rpm\""
RUN cd /home/interdev/ && rpm -ivh jdk-7u79-linux-x64.rpm
RUN alternatives --install /usr/bin/java java /usr/java/jdk1.7.0_79/jre/bin/java 2000000
ENV JAVA_HOME /usr/java/jdk1.7.0_79/jre"
RUN echo 'export JAVA_HOME=/usr/java/jdk1.7.0_79/jre' >> /root/.bashrc

#Add Maven
RUN cd /home/interdev && wget http://www-eu.apache.org/dist/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz && tar xzf apache-maven-3.3.9-bin.tar.gz && ln -s apache-maven-3.3.9 maven
ENV M2_HOME /home/interdev/maven
ENV PATH=${M2_HOME}/bin:${PATH}
RUN echo 'export M2_HOME=/home/interdev/maven' >> /root/.bashrc
RUN echo 'export PATH=${M2_HOME}/bin:${PATH}' >> /root/.bashrc

#Add Git
RUN yum install -y git

#Setting git ssh-key
RUN mkdir /root/.ssh/
ADD resource/id_rsa /root/.ssh/id_rsa
RUN chmod 700 /root/.ssh/id_rsa

# Create known_hosts
RUN touch /root/.ssh/known_hosts

# Add bitbuckets key
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

#Add Apache 2.4
RUN yum -y install httpd
ADD resource/httpd.conf /etc/httpd/conf/httpd.conf

#Add Tomcat 7
RUN yum install -y tomcat
RUN yum install -y tomcat-webapps tomcat-admin-webapps
RUN sed -i 's/\/usr\/lib\/jvm\/jre/\/usr\/java\/jdk1.7.0_79\/jre/g' /etc/tomcat/tomcat.conf
ADD resource/server.xml /usr/share/tomcat/conf/server.xml

#Git clone source
RUN mkdir /home/interdev/springbootsample
RUN git clone git@bitbucket.org:chauduc/springbootsample.git /home/interdev/springbootsample

#Build and deploy source
RUN /home/interdev/maven/bin/mvn -version
RUN cd /home/interdev/springbootsample && /home/interdev/maven/bin/mvn dependency:tree && /home/interdev/maven/bin/mvn clean package -DskipTests=true
RUN cp /home/interdev/springbootsample/target/spring.war /usr/share/tomcat/webapps/

#Start tomcat service
RUN systemctl enable tomcat.service
RUN systemctl enable httpd.service

#Open port 80,443, 8080
EXPOSE 80 443 8080

CMD ["/usr/sbin/init"]

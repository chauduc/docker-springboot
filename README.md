# README
* プロジェクトのインフラの構造のように開発環境はdockerで作成する。

## 設定方法

### Dockerインストール
https://www.docker.com/community-edition#/download

まず、Dockerをご設定ください.

Dockerインストール方法は上記のリンクでご参考ください。個人的に開発してから、CEバッションをお使いください。

### ビルド
Dockerインストールした後、 Dockerコマンドで下記のコマンドをご実行ください。


```docker run --privileged -v /sys/fs/cgroup:/sys/fs/cgroup:ro -p 80:80 -p 8080:8080 -itd chauduc/docker-springboot```

```docker ps```

DockerコンテナIDを習得して、下記のコマンドをご実行ください。

```docker exec -it DockerコンテナID /bin/bash```

プロジェクトのRootバッツ: /home/interdev/springbootsample

バソコンのブラウザで*http://localhost/spring*にアクセスできるようになります。

### コンテナの中にインストールした技術
|   | バーション  | 
|---|---|
|  CentOS | 7 |
|  Java | 1.7 |
|  Apache | 2.4 |
|  Tomcat | 7 |

## エラー対応
①下記のエラーを発生すれば、バソコンで80をポートするかご確認ください。

```docker: Error response from daemon: driver failed programming external connectivity on endpoint jovial_stallman 

(02607ddc9f3f02e63e0bff44f84bc769d25601ba5b116e7b2d84b36b5e408c1f): Error starting userland proxy: 

Bind for 0.0.0.0:80: unexpected error (Failure EADDRINUSE).```

## お問い合わせ

* chauduc.1211@gmail.com